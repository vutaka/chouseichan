select
  e.event_id,
  e.event_name,
  e.description,
  c.candidate_id,
  p.participant_id,
  p.participant_name,
  v.answer,
  p.comment
from
  event e
left join
  candidate c
on
  e.event_id = c.event_id
left join
  vote v
on
  c.candidate_id = v.candidate_id
left join
  participant p
on
  v.participant_id = p.participant_id
where
  e.event_id = /* eventId */'1'
order by
  p.participant_id
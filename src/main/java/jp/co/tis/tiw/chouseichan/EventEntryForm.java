package jp.co.tis.tiw.chouseichan;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

public class EventEntryForm {

    @NotBlank
    private String eventName;

    private String description;

    @NotBlank
    private String candidates;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCandidates() { return candidates; }



    public void setCandidates(String candidates) { this.candidates = candidates; }

    public List<String> getCandidateDateTimes() {
        return candidates.lines()
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }
}

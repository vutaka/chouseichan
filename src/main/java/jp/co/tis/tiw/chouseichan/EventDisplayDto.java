package jp.co.tis.tiw.chouseichan;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class EventDisplayDto extends Event {

    private List<Map<String, Object>> columns = Collections.emptyList();

    private List<Map<String, Object>> dataSource = Collections.emptyList();

    public List<Map<String, Object>> getColumns() {
        return columns;
    }

    public void setColumns(List<Map<String, Object>> columns) {
        this.columns = columns;
    }

    public List<Map<String, Object>> getDataSource() {
        return dataSource;
    }

    public void setDataSource(List<Map<String, Object>> dataSource) {
        this.dataSource = dataSource;
    }
}

package jp.co.tis.tiw.chouseichan;


import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.List;
import java.util.Map;

/**
 * 候補DAO
 */
@ConfigAutowireable
@Dao
public interface CandidateDao {

    /**
     * 候補を挿入する。
     *
     * @param candidate 候補
     * @return 挿入件数
     */
    @Insert
    int insert(Candidate candidate);

    /**
     * 指定したイベントIDの候補を取得する。
     * @param eventId イベントID
     * @return イベント
     */
    @Select
    List<Map<String, Object>> selectByEventId(Integer eventId);
}
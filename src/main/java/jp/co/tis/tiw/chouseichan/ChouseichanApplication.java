package jp.co.tis.tiw.chouseichan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 調整ちゃんアプリケーション
 */
@SpringBootApplication
public class ChouseichanApplication {

	/**
	 * メインメソッド。
	 * @param args 使用しない
	 */
	public static void main(String[] args) {
		SpringApplication.run(ChouseichanApplication.class, args);
	}

}

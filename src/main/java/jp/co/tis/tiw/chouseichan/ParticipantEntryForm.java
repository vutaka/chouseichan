package jp.co.tis.tiw.chouseichan;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ParticipantEntryForm {

    @NotBlank
    private String participantName;

    private String comment;

    @Valid
    @NotNull
    private List<VoteForm> voteForms;

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<VoteForm> getVoteForms() {
        return voteForms;
    }

    public void setVoteForms(List<VoteForm> voteForms) {
        this.voteForms = voteForms;
    }
}

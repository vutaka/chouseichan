package jp.co.tis.tiw.chouseichan;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 特定のURLにリクエストが来た場合に、index.htmlを返却するコントローラークラス。
 *
 * 今回のハンズオンでは、静的リソースもSpring Bootで管理するため、
 * フロントエンドのルーティングで使用するURLにアクセスがあった場合は、
 * {@literal /index.html}を返却する必要がある。
 */
@Controller
public class DummyController {

    /**
     * index.htmlを返却する。
     * @return index.html
     */
    @GetMapping(path = "/event")
    public String event() {
        return "/index.html";
    }

    /**
     * index.htmlを返却する。
     * @return index.html
     */
    @GetMapping(path = "/event/{id}")
    public String eventDetail() {
        return "/index.html";
    }
}

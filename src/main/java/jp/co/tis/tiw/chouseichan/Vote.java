package jp.co.tis.tiw.chouseichan;

import org.seasar.doma.Entity;

@Entity
public class Vote {

    private Integer participantId;

    private Integer candidateId;

    private String answer;

    public Integer getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Integer participantId) {
        this.participantId = participantId;
    }

    public Integer getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Integer candidateId) {
        this.candidateId = candidateId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

}

package jp.co.tis.tiw.chouseichan;

import javax.validation.constraints.NotBlank;

public class VoteForm {

    @NotBlank
    private String candidateId;

    private String answer;

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}

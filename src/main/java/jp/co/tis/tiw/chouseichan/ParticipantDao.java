package jp.co.tis.tiw.chouseichan;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.boot.ConfigAutowireable;

@ConfigAutowireable
@Dao
public interface ParticipantDao {

    @Insert
    int insert(Participant participant);
}

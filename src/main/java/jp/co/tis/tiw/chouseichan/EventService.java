package jp.co.tis.tiw.chouseichan;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class EventService {

    private EventDao eventDao;

    private CandidateDao candidateDao;

    private ParticipantDao participantDao;

    private VoteDao voteDao;

    /**
     * コンストラクタ。
     * @param eventDao イベントDAO
     * @param candidateDao 候補日程DAO
     * @param participantDao 参加者DAO
     * @param voteDao 投票DAO
     */
    public EventService(EventDao eventDao, CandidateDao candidateDao, ParticipantDao participantDao,
            VoteDao voteDao) {
        this.eventDao = eventDao;
        this.candidateDao = candidateDao;
        this.participantDao = participantDao;
        this.voteDao = voteDao;
    }

    /**
     * イベントを新規登録する。
     *
     * @param eventName イベント名
     * @param description 説明
     * @param dateTimes 日程
     * @return 登録されたイベント
     */
    public Event registerNewEvent(String eventName, String description, List<String> dateTimes) {
        Event event = new Event();
        event.setEventName(eventName);
        event.setDescription(description);
        eventDao.insert(event);

        for (String dateTime : dateTimes){
            Candidate candidate = new Candidate();
            candidate.setEventId(event.getEventId());
            candidate.setDateTime(dateTime);
            candidateDao.insert(candidate);
        }
        return event;
    }

    /**
     * イベントを取得する。
     * @param eventId イベントID
     * @return イベント表示用DTO
     */
    public EventDisplayDto getEvent(Integer eventId) {
        List<EventDetailDto> eventDetails = eventDao.selectById(eventId);
        if (eventDetails.isEmpty()) {
            return null;
        }
        EventDetailDto event = eventDetails.get(0);

        List<Map<String, Object>> columns = new ArrayList<>();
        Map<String, Object> column = new HashMap<>();
        column.put("title", "名前");
        column.put("dataIndex", "name");
        column.put("key", "name");
        columns.add(column);
        columns.addAll(candidateDao.selectByEventId(eventId));

        EventDisplayDto dto = new EventDisplayDto();
        dto.setColumns(columns);

        String eventName = event.getEventName();
        String description = event.getDescription();
        dto.setEventId(eventId);
        dto.setEventName(eventName);
        dto.setDescription(description);

        if (event.getParticipantId() == null) {
            return dto;
        }

        List<Map<String, Object>> datasources = new ArrayList<>();
        Map<Integer, List<EventDetailDto>> group = eventDetails.stream()
                .collect(Collectors.groupingBy(EventDetailDto::getParticipantId));
        List<Integer> keys = eventDetails.stream().map(EventDetailDto::getParticipantId).distinct()
                .collect(Collectors.toList());
        int keyIndex = 1;
        for (Integer key : keys) {
            List<EventDetailDto> list = group.get(key);
            Map<String, Object> datasource = new HashMap<>();
            datasource.put("key", keyIndex++);
            datasource.put("name", list.get(0).getParticipantName());
            datasource.putAll(list.stream().collect(
                    Collectors.toMap(a -> a.getCandidateId().toString(), EventDetailDto::getAnswer)));
            datasources.add(datasource);
        }

        dto.setDataSource(datasources);
        return dto;
    }

    /**
     * 参加可否を新規登録する。
     * @param participantName 参加者名
     * @param comment コメント
     * @param voteForms 参加可否フォーム
     */
    public void registerNewParticipant(String participantName, String comment, List<VoteForm> voteForms) {
        Participant participant = new Participant();
        participant.setParticipantName(participantName);
        participant.setComment(comment);
        participantDao.insert(participant);

        for (VoteForm voteForm : voteForms){
            Vote vote = new Vote();
            vote.setParticipantId(participant.getParticipantId());
            vote.setCandidateId(Integer.parseInt(voteForm.getCandidateId()));
            vote.setAnswer(voteForm.getAnswer());
            voteDao.insert(vote);
        }
    }
}

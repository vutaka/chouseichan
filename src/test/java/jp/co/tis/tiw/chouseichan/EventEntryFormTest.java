package jp.co.tis.tiw.chouseichan;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EventEntryFormTest {

    EventEntryForm sut = new EventEntryForm();

    @Test
    public void getCandidateDateで改行コードで分割され_空行は取り除かれること() {
        sut.setCandidates("1/1\n1/2\n\n1/3");
        assertEquals(
                Arrays.asList("1/1", "1/2", "1/3"),
                sut.getCandidateDateTimes());
    }

    @Test
    public void getCandidateDateで単一の値が取得できること() {
        sut.setCandidates("1/1");
        assertEquals(
                Arrays.asList("1/1"),
                sut.getCandidateDateTimes());
    }

}
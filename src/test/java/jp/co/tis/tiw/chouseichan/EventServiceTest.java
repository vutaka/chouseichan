package jp.co.tis.tiw.chouseichan;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;

class EventServiceTest {

    private EventService eventService;
    private EventDao eventDao;
    private CandidateDao candidateDao;
    private ParticipantDao participantDao;
    private VoteDao voteDao;

    @BeforeEach
    void setUp() {
        candidateDao = mock(CandidateDao.class);
        eventDao = mock(EventDao.class);
        participantDao = mock(ParticipantDao.class);
        voteDao = mock(VoteDao.class);
        eventService = new EventService(eventDao, candidateDao, participantDao, voteDao);
    }

    @Test
    void registerNewEvent() throws Exception {

        ArgumentMatcher<Event> event1 = a -> a != null
                && Objects.equals(a.getEventName(), "aaa")
                && Objects.equals(a.getDescription(), "bbb");
        given(eventDao.insert(argThat(event1))).willAnswer(invocation -> {
            Event a = invocation.getArgument(0);
            a.setEventId(1);
            return 1;
        });

        ArgumentMatcher<Candidate> candidate1 = a -> a != null
                && Objects.equals(a.getEventId(), 1)
                && Objects.equals(a.getDateTime(), "ccc");
        given(candidateDao.insert(argThat(candidate1))).willAnswer(invocation -> {
            Candidate a = invocation.getArgument(0);
            a.setCandidateId(1);
            return 1;
        });

        ArgumentMatcher<Candidate> candidate2 = a -> a != null
                && Objects.equals(a.getEventId(), 1)
                && Objects.equals(a.getDateTime(), "ddd");
        given(candidateDao.insert(argThat(candidate2))).willAnswer(invocation -> {
            Candidate a = invocation.getArgument(0);
            a.setCandidateId(2);
            return 1;
        });

        String eventName = "aaa";
        String description = "bbb";
        List<String> dateTimes = Arrays.asList("ccc", "ddd" );
        Event event2 = eventService.registerNewEvent(eventName, description, dateTimes);

        assertEquals((Integer) 1, event2.getEventId());

        verify(eventDao).insert(argThat(event1));
        verify(candidateDao).insert(argThat(candidate1));
        verify(candidateDao).insert(argThat(candidate2));
    }

    @Test
    void getEvent_候補日が1個_参加者が1名() throws Exception {
        EventDetailDto event1 = new EventDetailDto();
        event1.setEventName("aaa");
        event1.setDescription("bbb");

        event1.setParticipantId(2);
        event1.setParticipantName("ccc");
        event1.setCandidateId(3);
        event1.setAnswer("ddd");

        given(eventDao.selectById(1)).willReturn(Collections.singletonList(event1));

        List<Map<String, Object>> value = List.of(
                Map.of("dataIndex", 3, "key", 3, "title", "eee"));
        given(candidateDao.selectByEventId(1)).willReturn(value);

        EventDisplayDto event2 = eventService.getEvent(1);

        assertEquals((Integer) 1, event2.getEventId());
        assertEquals("aaa", event2.getEventName());
        assertEquals("bbb", event2.getDescription());
        assertEquals(List.of(Map.of("title", "名前", "dataIndex", "name", "key", "name"),
                Map.of("dataIndex", 3, "key", 3, "title", "eee")), event2.getColumns());
        assertEquals(List.of(Map.of("3", "ddd", "name", "ccc", "key", 1)), event2.getDataSource());
    }

    @Test
    void getEvent_イベントが存在しない() throws Exception {
        given(eventDao.selectById(1)).willReturn(Collections.emptyList());
        EventDisplayDto event2 = eventService.getEvent(1);
        assertNull(event2);
    }

    @Test
    void getEvent_参加者が0名() throws Exception {
        EventDetailDto event1 = new EventDetailDto();
        event1.setEventName("aaa");
        event1.setDescription("bbb");

        given(eventDao.selectById(1)).willReturn(Collections.singletonList(event1));

        List<Map<String, Object>> value = List.of(
                Map.of("dataIndex", 3, "key", 3, "title", "eee"),
                Map.of("dataIndex", 4, "key", 4, "title", "ppp"),
                Map.of("dataIndex", 5, "key", 5, "title", "qqq"));
        given(candidateDao.selectByEventId(1)).willReturn(value);

        EventDisplayDto event2 = eventService.getEvent(1);

        assertEquals((Integer) 1, event2.getEventId());
        assertEquals("aaa", event2.getEventName());
        assertEquals("bbb", event2.getDescription());
        assertEquals(List.of(
                Map.of("title", "名前", "dataIndex", "name", "key", "name"),
                Map.of("dataIndex", 3, "key", 3, "title", "eee"),
                Map.of("dataIndex", 4, "key", 4, "title", "ppp"),
                Map.of("dataIndex", 5, "key", 5, "title", "qqq")), event2.getColumns());
        assertEquals(Collections.emptyList(), event2.getDataSource());
    }

    @Test
    void getEvent_候補日が3個_参加者が3名() throws Exception {
        EventDetailDto event1 = new EventDetailDto();
        event1.setEventName("aaa");
        event1.setDescription("bbb");
        event1.setParticipantId(2);
        event1.setParticipantName("ccc");
        event1.setCandidateId(3);
        event1.setAnswer("ddd");

        EventDetailDto event2 = new EventDetailDto();
        event2.setEventName("aaa");
        event2.setDescription("bbb");
        event2.setParticipantId(2);
        event2.setParticipantName("ccc");
        event2.setCandidateId(4);
        event2.setAnswer("fff");

        EventDetailDto event3 = new EventDetailDto();
        event3.setEventName("aaa");
        event3.setDescription("bbb");
        event3.setParticipantId(2);
        event3.setParticipantName("ccc");
        event3.setCandidateId(5);
        event3.setAnswer("ggg");

        EventDetailDto event4 = new EventDetailDto();
        event4.setEventName("aaa");
        event4.setDescription("bbb");
        event4.setParticipantId(6);
        event4.setParticipantName("hhh");
        event4.setCandidateId(3);
        event4.setAnswer("iii");

        EventDetailDto event5 = new EventDetailDto();
        event5.setEventName("aaa");
        event5.setDescription("bbb");
        event5.setParticipantId(6);
        event5.setParticipantName("hhh");
        event5.setCandidateId(4);
        event5.setAnswer("jjj");

        EventDetailDto event6 = new EventDetailDto();
        event6.setEventName("aaa");
        event6.setDescription("bbb");
        event6.setParticipantId(6);
        event6.setParticipantName("hhh");
        event6.setCandidateId(5);
        event6.setAnswer("kkk");

        EventDetailDto event7 = new EventDetailDto();
        event7.setEventName("aaa");
        event7.setDescription("bbb");
        event7.setParticipantId(7);
        event7.setParticipantName("lll");
        event7.setCandidateId(3);
        event7.setAnswer("mmm");

        EventDetailDto event8 = new EventDetailDto();
        event8.setEventName("aaa");
        event8.setDescription("bbb");
        event8.setParticipantId(7);
        event8.setParticipantName("lll");
        event8.setCandidateId(4);
        event8.setAnswer("nnn");

        EventDetailDto event9 = new EventDetailDto();
        event9.setEventName("aaa");
        event9.setDescription("bbb");
        event9.setParticipantId(7);
        event9.setParticipantName("lll");
        event9.setCandidateId(5);
        event9.setAnswer("ooo");

        given(eventDao.selectById(1)).willReturn(
                List.of(event1, event2, event3, event4, event5, event6, event7, event8, event9));

        List<Map<String, Object>> value = List.of(
                Map.of("dataIndex", 3, "key", 3, "title", "eee"),
                Map.of("dataIndex", 4, "key", 4, "title", "ppp"),
                Map.of("dataIndex", 5, "key", 5, "title", "qqq"));
        given(candidateDao.selectByEventId(1)).willReturn(value);

        EventDisplayDto event10 = eventService.getEvent(1);

        assertEquals((Integer) 1, event10.getEventId());
        assertEquals("aaa", event10.getEventName());
        assertEquals("bbb", event10.getDescription());
        assertEquals(List.of(
                Map.of("title", "名前", "dataIndex", "name", "key", "name"),
                Map.of("dataIndex", 3, "key", 3, "title", "eee"),
                Map.of("dataIndex", 4, "key", 4, "title", "ppp"),
                Map.of("dataIndex", 5, "key", 5, "title", "qqq")), event10.getColumns());
        assertEquals(List.of(
                Map.of("3", "ddd", "name", "ccc", "key", 1, "4", "fff", "5", "ggg"),
                Map.of("3", "iii", "name", "hhh", "key", 2, "4", "jjj", "5", "kkk"),
                Map.of("3", "mmm", "name", "lll", "key", 3, "4", "nnn", "5", "ooo")),
                event10.getDataSource());
    }

    @Test
    void registerNewParticipant() throws Exception {

        ArgumentMatcher<Participant> participant1 = a -> a != null
                && Objects.equals(a.getParticipantName(), "aaa")
                && Objects.equals(a.getComment(), "bbb");
        given(participantDao.insert(argThat(participant1))).willAnswer(invocation -> {
            Participant a = invocation.getArgument(0);
            a.setParticipantId(1);
            return 1;
        });

        ArgumentMatcher<Vote> vote1 = a -> a != null
                && Objects.equals(a.getParticipantId(), 1)
                && Objects.equals(a.getCandidateId(), 2)
                && Objects.equals(a.getAnswer(), "ccc");
        given(voteDao.insert(argThat(vote1))).willReturn(1);

        ArgumentMatcher<Vote> vote2 = a -> a != null
                && Objects.equals(a.getParticipantId(), 1)
                && Objects.equals(a.getCandidateId(), 3)
                && Objects.equals(a.getAnswer(), "ddd");
        given(voteDao.insert(argThat(vote2))).willReturn(1);

        String participantName = "aaa";
        String comment = "bbb";
        List<VoteForm> voteForms = new ArrayList<>();
        VoteForm voteForm1 = new VoteForm();
        voteForm1.setCandidateId("2");
        voteForm1.setAnswer("ccc");
        voteForms.add(voteForm1);
        VoteForm voteForm2 = new VoteForm();
        voteForm2.setCandidateId("3");
        voteForm2.setAnswer("ddd");
        voteForms.add(voteForm2);
        eventService.registerNewParticipant(participantName, comment, voteForms);

        verify(participantDao).insert(argThat(participant1));
        verify(voteDao).insert(argThat(vote1));
        verify(voteDao).insert(argThat(vote2));
    }
}

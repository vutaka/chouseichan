import React from 'react';
import { withRouter } from 'react-router';
import { Form, Input, Button, Typography } from 'antd';

const { Title } = Typography,
    {TextArea} = Input;


class EventEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // ここにstateの初期値を書きます
            eventName: "",
            description: "",
            candidates: ""
        };
    }

    registerEvent = (evt) => {
        evt.preventDefault();
        // ここにサーバーにデータを送信する処理を書きます。
        fetch('/api/events/', {
            method: 'POST',
            body: JSON.stringify(this.state), // data can be `string` or {object}!
            headers:{
              'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .then(json => this.props.history.push(`event/${json.eventId}`))
    }

    onChangeEventName = (evt) => {
        // 入力された値(evt.target.value)をsetStateします
        const eventName = evt.target.value;
        this.setState({eventName: eventName});
    }

    onChangeDescription = (evt) => {
        const description = evt.target.value;
        this.setState({description: description});
    }

    onChangeCandidates = (evt) => {
        const candidates = evt.target.value;
        this.setState({candidates: candidates});
    }

    // <div>内に入力フォームを作ります
    render() {
        return (
            <div>
                <Title level={2}>イベント登録</Title>
                <Form>
                    <Form.Item label="イベント名">
                        <Input placeholder="送別会" onChange={this.onChangeEventName} value={this.state.eventName} />
                    </Form.Item>
                    <Form.Item label="説明">
                        <TextArea placeholder="送別会の日程を調整しましょう！出欠" onChange={this.onChangeDescription} value={this.state.description} />
                    </Form.Item>
                    <Form.Item label="候補日程">
                        <TextArea placeholder="8/7(月)20:00～" onChange={this.onChangeCandidates} value={this.state.candidates}/>
                    </Form.Item>
                    <Button type="primary" onClick={this.registerEvent}>出欠表を作る</Button>
                </Form>
            </div>
        );
    }

}

export default withRouter(EventEntry);
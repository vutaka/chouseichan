# 時間が余ったら

もし時間が余ったら、何かアプリケーションの改良をしてみましょう。

例えば「URLを参加者に伝えるためのTwitterボタンを追加する」というのは、
簡単で実用的な改良になると思われます。

- [自分のウェブサイトにTwitterボタンを追加する方法](https://help.twitter.com/ja/using-twitter/twitter-buttons)


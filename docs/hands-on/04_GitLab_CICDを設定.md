# GitLabのCI/CDを設定する

GitLabのCI/CDを設定し、masterブランチにpushされたら、
自動でアプリケーションをビルド、デプロイするように設定しましょう。


## Heroku API Keyを取得する

CI/CDからHerokuにデプロイするために、Heroku API Keyが必要となります。

https://dashboard.heroku.com/account
にアクセスします。

下の方にスクロールすると、`API Key`というセクションがあります。
`Reveal`ボタンを押下すると、APIキーが表示されますので、これをコピーします。

![APIKEY](./img/heroku_api_key.png)

## Heroku API KeyをGitLab CI/CDに登録する

APIキーを設定ファイル等に書いてしまうと、APIキーが流出してしまいます。
これを防ぐため、Webコンソール上からGitLab CI/CDの環境変数にAPIキーを登録します。

GitLabにログインし、左側のメニューから`設定`->`CI/CD`を選択します。
環境変数というセクションがあります。`展開`ボタンを押すと環境変数が入力できるようになります。

以下のように、環境変数を登録します。

| キー           | 値                  |
|----------------|---------------------|
| HEROKU_API_KEY | <コピーしたAPIキー> |


![env](./img/gitlab_env.png)

ここに登録しておくと、CI/CD実行時に環境変数に設定されます。


## .gitlab-ci.ymlを作成する

ローカルリポジトリのmasterブランチにて、
プロジェクト直下に`.gitlab-ci.yml`というファイルを作成します。
このファイルにGitLab CI/CDで実行するジョブを記述します。

``` yaml
image:  maven:3.6.0-jdk-11

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

cache:
  paths:
    - .m2/repository/

test:
  stage: test
  script:
    - mvn clean test
  only:
    - develop

deploy:
  stage: deploy
  script:
    - mvn -P production clean heroku:deploy
  only:
    - master
```

上記の例では、developブランチにpushされたらテストを実行、
masterブランチにpushされたらデプロイを行うように設定しています。

## pushする

ファイルが完成したらcommit、pushします。
masterブランチにpushをしたので、デプロイが実行されます。

GitLabの左側のメニュから`CI/CD`をクリックするとCI/CDの実行状況が確認できます。

![pipeline](./img/gitlab_pipeline.png)

# イベント詳細を表示する

指定したURL(例: https://chuseichan.herokuapp.com/event/1 )に対応するイベントの詳細情報を表示する箇所を作成します。



### 画面イメージ

![画面イメージ](./img/イベント詳細画面.png)


※画面下部の参加者が参加可否を登録する機能は実装済みです

### 対象ファイル

- <project_root>/front/src/Event.js


## イベント詳細情報をサーバから取得しよう


http://localhost:3000/event/1
にアクセスしてみてください。

初期状態ではサーバにアクセスせず、ローカルのダミーデータ(`src/sampleData.js`)を表示するようになっています。
ダミーデータではなく、サーバから取得したデータを表示するようにしましょう。

### WebAPIのURLを組み立てよう

送信するリクエストは以下の通りです。


| URL             | メソッド | Content-Type     |
|-----------------|----------|------------------|
| /api/events/:id | GET      | application/json |

URLに`イベントID`を含める必要があるため、この値を取得する必要があります。
このページは以下のようなルーティング設定になっています。

```
<Route exact path='/event/:id' component={Event} />
```

URL末尾のIDは、以下のようにReact RouterのAPIから取得できます(propsに自動的に設定されます)。

``` javascript
this.props.match.params.id
```

### サーバから情報を取得しよう


以下の箇所にサーバからデータを取得する処理を追加していきます。
``` javascript
fetchEventDetail = () => {
    // ここに記載
}
```

この関数は[componentDidMount](https://reactjs.org/docs/react-component.html#componentdidmount)から呼び出しているので、
画面遷移した時点で自動的に呼び出されます。


[Fetch API](https://developer.mozilla.org/ja/docs/Web/API/Fetch_API/Using_Fetch)を使用して、データを取得しましょう。


### 取得したデータをstateに格納しよう

イベントIDが1のデータが登録済みです。
http://localhost:8080/api/events/1

にアクセスすると、以下のようなデータが返却されます。

``` javascript
{
  "eventId": 1,
  "eventName": "送別会",
  "description": "送別会やります。",
  "columns": [
    {
      "dataIndex": "name",
      "title": "名前",
      "key": "name"
    },
    {
      "dataIndex": 1,
      "key": 1,
      "title": "2019/03/18"
    },
    {
      "dataIndex": 2,
      "key": 2,
      "title": "2019/03/19"
    },
    {
      "dataIndex": 3,
      "key": 3,
      "title": "2019/03/20"
    }
  ],
  "dataSource": [
    {
      "name": "一郎",
      "key": 1
      "1": "○",
      "2": "×",
      "3": "△",
    },
    {
      "name": "次郎",
      "key": 2
      "1": "△",
      "2": "○",
      "3": "×",
    },
  ]
}
```


ハンズオンでの実装を簡単にするため、加工済みのデータが返却されるようになっています。
`render`でデータが表示するために、返却されたデータを`setState`に格納しておきましょう。

stateの構造については、`constructor`を参照してください。
サーバから取得したデータを、`eventDetail`に設定します。

[コードイメージ]
``` javascript
const json // サーバから取得したJSON
this.setState({
   eventDetail: json
}
```

### stateに格納した値を表示しよう

| 表示項目           | 対応するプロパティ      |
|--------------------|-------------------------|
| イベント名         | eventDetail.eventName   |
| 説明               | eventDetail.description |
| 出欠表(カラム定義) | eventDetail.columns     |
| 出欠表(データ)     | eventDetail.dataSource  |


`columns`と`dataSource`はそのままAnt Designの[Table](https://ant.design/components/table/)に設定できます。

コード例
``` jsx
<Table dataSource={this.state.eventDetail.dataSource} columns={this.state.eventDetail.columns} pagination={false} />
```

## 候補日程を表示用に編集する

画面下段の○×△のボタンを描画するため、サーバから取得したデータを編集します。

サーバから取得したデータに候補日程の情報が含まれていました。

``` json
  "columns": [
    {
      "dataIndex": "name",
      "title": "名前",
      "key": "name"
    },
    {
      "dataIndex": 1,
      "key": 1,
      "title": "2019/03/18"
    },
    {
      "dataIndex": 2,
      "key": 2,
      "title": "2019/03/19"
    },
    {
      "dataIndex": 3,
      "key": 3,
      "title": "2019/03/20"
    }
  ],
```

これを`sampleData.js`の以下のようなデータに変換します。

``` javascript
export const candidatesSample = [
    {
        id: 1,
        dateTime: "2019/1/1",
        answer: '△'
    },
    {
        id: 2,
        dateTime: "2019/1/2",
        answer: '△'
    },
    {
        id: 3,
        dateTime: "2019/1/3",
        answer: '△'
    }
];
```

まとめると以下のような編集仕様になります。

| 編集元 | 編集先   | 備考                     |
|--------|----------|--------------------------|
| key    | id       | 候補日程を特定するIDです |
| title  | dateTime | 候補日程                 |
| -      | answer   | '△'固定(初期値)         |


あるデータの配列を元に新しいデータの配列を作るには[map関数](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Global_Objects/Array/map)を使います。

以下の例では、元オブジェクトのnameに'さん'を付加した新しいオブジェクトの配列を作成しています。

``` javascript
const orig = [{ name: '山田' }, { name: '田中'}];
const san = orig.map(member => {
    return {
        name: member.name + 'さん';
    }
});
console.log(san);
[ { name: '山田さん' }, { name: '田中さん' } ]
```

変換ができたら、その後の`setState`に変換後の値が渡るようにしてください。

``` javascript

        const candidate = ... // 変換後の候補日程
        
        this.setState({
            eventDetail,
            participant: {
                ...this.state.participant,
                candidates
            }
        });
```

うまく変換できていれば、候補日程が表示されます。

[Herokuへデプロイ](./03_Herokuへデプロイ.md)に進んでください。

title: ハンズオン導入
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  {{title}}
]

---

class: impact

# {{title}}
## 

---

# ハンズオンコンテンツの説明

1. Reactアプリケーションを作成する
  1. イベントを登録する
  1. 参加状況を確認する
  1. 参加可否を登録する
1. Herokuにデプロイしてみる(手動)
1. GitLab CI/CDを利用して自動デプロイしてみる

---

# アプリケーションの説明

## つくるもの

- [調整さん](https://chouseisan.com/)を模倣したアプリケーションを作成します

## 完成イメージ

- [調整ちゃん](https://chouseichan.herokuapp.com/event/)

---

# デプロイ, CI/CD

## Heroku

- ローカルPC本番用のjarをビルドし、それをHerokuにデプロイしてみます。
  - この時点で、とりあえずインターネット公開まではできたことになります！

## CI/CD

- GitLab CI/CDを利用して、masterブランチにpushされたらHerokuにデプロイする設定を入れてみます

---

# 今日の成果イメージ

- Reactでアプリを作って、
- CI/CDを使って、
- インターネットに公開する！

---

# 成果イメージを見てみよう

- [アプリ](https://chouseichan.herokuapp.com/event)
- [Heroku](https://dashboard.heroku.com/apps/chouseichan/activity)
- [GitLab CI/CD](https://gitlab.com/sambatriste/chouseichan)

# タイムスケジュール

- 〜15:00  Reactアプリケーション作成
- 〜15:15  Herokuデプロイ(手動)
- 〜15:30  GitLab CI/CD設定
- 〜16:45  クロージング
- 16:45〜  アンケート回答、流れ解散

---

# ハンズオンの実施方法

- ガイドに沿って自分のペースで進めてください
  - 
- 時間がたりなさそうな時は、`develop`ブランチのコードを参考にしてください
- 時間があまりそうなら、自分のアイデアでアプリを改善してみてください
- その他不明点や相談事がありましたらスタッフにお声がけください

---

# 時間配分のめやす

- 15:00くらいまでにアプリのほうを仕上げましょう
  - 1機能45分くらいを目処に(45min * 2 = 90min)
- 15:00くらいからデプロイ、CI/CDに取り掛かりましょう
  - アプリが間に合わない場合は、developブランチを使ってください
  - それぞれ15分くらいを目処に(15min * 2 = 30min)



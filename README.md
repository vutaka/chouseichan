# 調整ちゃん

## これはなに？

ハンズオン学習用に用意されたスケジュール調整アプリケーションです。
[調整さん](https://chouseisan.com/)のスペックを模倣しています。

## 事前準備

ハンズオンに先立って、事前準備をお願いします。

- [インストールとサインアップ](./docs/env/install-and-signup.md)
- [インストール後の動作確認](./docs/env/post-install-check.md)

## 導入説明

作業前に、当日の流れ、進め方を説明します。

- [ハンズオン導入](./docs/introduction/introduction.md)


## ハンズオン作業

### 前準備

- [前準備](docs/hands-on/00_前準備.md)

### ハンズオン

- [イベントを登録する](docs/hands-on/01_イベントを登録する.md) (～45分)
- [イベント詳細を表示する](docs/hands-on/02_イベント詳細を表示する.md) (～45分)
- [Herokuへデプロイ](docs/hands-on/03_Herokuへデプロイ.md) (～15分)
- [GitLab_CI/CDを設定](docs/hands-on/04_GitLab_CICDを設定.md) (～15分)

